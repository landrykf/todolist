<?php

class Task 
{
    private $name;
    private $category;

    public function __construct(string $name, string $category)
    {
        $this->name = $name;
        $this->category = $category;
    }


    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        return $this->name = $name;

        return $this;
    }


    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory(string $category)
    {
        return $this->category = $category;

        return $this;
    }
}